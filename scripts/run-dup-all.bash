#!/bin/bash

for i in ../raw-csv/*.csv; do
    python dup-parent-rows.py $i > `echo $i | sed 's/\.csv/\-dup\.csv/' | sed 's/raw-csv/csv/'`
done

python dup-parent-rows.py ../raw-csv/*.csv --skip=2 > ../csv/all-dup.csv
