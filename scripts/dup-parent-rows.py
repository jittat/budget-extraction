import csv
import sys

def merge_row(row, prev_row):
    DATA_FIELD_COUNT = 10

    for i in range(DATA_FIELD_COUNT):
        if row[i] == '':
            row[i] = prev_row[i]
        else:
            break
    return row

def main():
    input_filenames = [f for f in sys.argv[1:] if not f.startswith('--')]

    skip_count = 0
    
    options = [f for f in sys.argv[1:] if f.startswith('--')]
    for opt in options:
        if opt.startswith('--skip='):
            skip_count = int(opt.split('=')[1])
    
    rows = []
    for f in input_filenames:
        with open(f) as csvfile:
            reader = csv.reader(csvfile)

            prev_row = None
            output_rows = []

            r = 0
            for row in reader:
                if prev_row:
                    row = merge_row(row, prev_row)

                r += 1
                if r > skip_count:
                    output_rows.append(row)

                prev_row = row
                
        rows += output_rows

    writer = csv.writer(sys.stdout)
    for r in rows:
        writer.writerow(r)


if __name__ == '__main__':
    main()
